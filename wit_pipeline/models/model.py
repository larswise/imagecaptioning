from __future__ import annotations

import logging
import typing

from tensorflow.python.keras.layers import TextVectorization
from tensorflow.keras.optimizers import Adam, SGD, RMSprop
from config import Configuration

import tensorflow as tf
import numpy as np
from tensorflow.keras import Model

from models.decoder import Decoder, DecoderInput
from models.encoder import Encoder


from models.loss import MaskedLoss


class CaptionModel(Model):

    def __init__(self, embedding_dim, img_shape, units, vectorizer: TextVectorization,
               vocab_size, config: Configuration, use_glove: bool = False): # consider adding tf func option
        super().__init__()
        logging.info(f"Use regularization: {config.use_regularization}")
        self.img_shape = img_shape
        self.use_regularization = config.use_regularization
        self.vectorizer = vectorizer
        self.vocab = vectorizer.get_vocabulary()
        self.encoder = Encoder(embedding_dim, config)
        self.decoder = Decoder(DecoderInput(vectorizer, units, embedding_dim, vocab_size, use_glove, config, config.use_regularization))

    @tf.function
    def tf_train_step(self, img_tensor, target, validation: bool = False):
        return self._train_step(img_tensor, target, validation)

    def train_step(self, img_tensor, target, validation: bool = False):
        return self._train_step(img_tensor, target, validation)


    def _train_step(self, img_tensor, target, validation: bool):
        loss = 0

        hidden = self.decoder.reset_state(batch_size=target.shape[0])

        # Init each element in the batch with the startseq token
        dec_input = tf.expand_dims([self.vocab.index('<START>')] * target.shape[0], 1)
        if not validation:
            with tf.GradientTape() as tape:
                features = self.encoder(img_tensor)
                for i in range(1, target.shape[1]):
                    predictions, hidden, _ = self.decoder(dec_input, features, hidden)
                    loss += self.loss(target[:, i], predictions)
                    dec_input = tf.expand_dims(target[:, i], 1)
                total_loss = (loss / int(target.shape[1]))

                trainable_variables = self.encoder.trainable_variables + self.decoder.trainable_variables
                gradients = tape.gradient(loss, trainable_variables)
                self.optimizer.apply_gradients(zip(gradients, trainable_variables))
        else:
            features = self.encoder(img_tensor)
            for i in range(1, target.shape[1]):
                predictions, hidden, _ = self.decoder(dec_input, features, hidden)
                current_target = tf.expand_dims(target[:, i], axis=1)
                loss += self.loss(current_target, predictions)
                dec_input = current_target
            total_loss = (loss / int(target.shape[1]))


        return loss, total_loss

    def evaluate(self, batched_images, max_len):
        attention_plot = np.zeros((max_len, self.img_shape))

        hidden = self.decoder.reset_state(batch_size=1)

        features = self.encoder(batched_images)

        dec_input = tf.expand_dims([self.vocab.index('<START>')], 0)
        result = []

        for i in range(max_len):
            predictions, hidden, attention_weights = self.decoder(dec_input, features, hidden)

            attention_plot[i] = tf.reshape(attention_weights, (-1, )).numpy()

            predicted_id = tf.argmax(predictions[0]).numpy()
            result.append(self.vocab[predicted_id])

            if self.vocab[predicted_id] == '<EOS>':
                return result, attention_plot

            dec_input = tf.expand_dims([predicted_id], 0)

        attention_plot = attention_plot[:len(result), :]
        return result, attention_plot

    @staticmethod
    def load_or_init(embedding_dim, img_shape, units, vectorizer, cfg: Configuration, train_data_dir, use_regularization: bool = False) -> typing.Tuple[CaptionModel, typing.Any, typing.Any]:
        model = CaptionModel(embedding_dim, img_shape, units, vectorizer,
                             vocab_size=(vectorizer.vocabulary_size()),
                             config=cfg,
                             use_glove=cfg.use_glove)  # add pad token
        if cfg.optimizer == 'adam':
            optimizer = Adam(learning_rate=cfg.learning_rate)
        elif cfg.optimizer == 'sgd':
            optimizer = SGD(learning_rate=cfg.learning_rate, momentum=0.9, decay=0.01)
        elif cfg.optimizer == 'rmsprop':
            optimizer = RMSprop(learning_rate=cfg.learning_rate)
        model.compile(optimizer=optimizer, loss=MaskedLoss())


        ckpt = tf.train.Checkpoint(encoder=model.encoder,
                                   decoder=model.decoder,
                                   optimizer=model.optimizer)

        ckpt_manager = tf.train.CheckpointManager(ckpt, train_data_dir, max_to_keep=5)

        start_epoch = 0

        if ckpt_manager.latest_checkpoint:
            start_epoch = int(ckpt_manager.latest_checkpoint.split('-')[-1])
            # restoring the latest checkpoint in checkpoint_path
            ckpt.restore(ckpt_manager.latest_checkpoint)
            logging.info(f"Model was restored from a checkpoint at epoch {start_epoch}")
        else:
            logging.info(f"No restore, new model")
        return model, ckpt_manager, start_epoch