import logging
import typing

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.python.keras.layers import TextVectorization

from config import Configuration
from models.attention import BahdanauAttention
from steps.step3_download_glove import load_glove_vectors
from steps.step4_text_processor import get_embeddings


class DecoderInput(typing.NamedTuple):
    vectorizer: TextVectorization
    dec_num_cells: int
    enc_dim: int
    vocab_size: int
    use_glove: bool
    config: Configuration
    regularize: bool
    #add the mask!

class Decoder(tf.keras.Model):
    def __init__(self,
                 inputs: DecoderInput):

        super(Decoder, self).__init__()
        self.vectorizer = inputs.vectorizer
        self.num_cells = inputs.dec_num_cells
        self.enc_dim = inputs.enc_dim
        self.vocab_size = inputs.vocab_size
        self.regularize = inputs.regularize
        logging.info(f"Recurrent Units {self.num_cells} - embedding / fc dim {self.enc_dim}")
        if self.regularize:
            logging.info(f'Adding regularization and dropout objects ({inputs.config.drop_amt})')
            fc1_reg = tf.keras.regularizers.L1(l1=inputs.config.regularization_amt)
            self.dropout = tf.keras.layers.Dropout(inputs.config.drop_amt)
        else:
            logging.info('Setting regularizers to None')
            fc1_reg = None

        if inputs.use_glove:
            glove_matrix = self.get_glove(inputs.config)
            self.embedding_layer = keras.layers.Embedding(
                glove_matrix.shape[0],
                inputs.config.glove_dim,
                embeddings_initializer=keras.initializers.Constant(glove_matrix),
                trainable=False
            )
        else:
            self.embedding_layer = keras.layers.Embedding(self.vocab_size, self.enc_dim)

        self.gru = keras.layers.GRU(units=self.num_cells,
                                    return_sequences=True,
                                    return_state=True,
                                    recurrent_initializer='glorot_uniform')

        self.fc1 = keras.layers.Dense(inputs.config.decoder_fc_neurons, activity_regularizer=fc1_reg)
        self.fc2 = keras.layers.Dense(self.vocab_size)
        self.attention = BahdanauAttention(self.num_cells)

    def get_glove(self, cfg: Configuration):
        vectors = load_glove_vectors(cfg)
        return get_embeddings(cfg, self.vectorizer.get_vocabulary(), vectors)

    def call(self, x, features, hidden):
        """
        :param x: caption
        :param features: image vector (flattened)
        :param hidden: hidden encoder state?
        """
        context, attention_weights = self.attention(features, hidden)
        # embed texts to glove vectors
        x = self.embedding_layer(x)
        output, state = self.gru(tf.concat([tf.expand_dims(context, 1), x], axis=-1))
        x = self.fc1(output)
        x = tf.reshape(x, (-1, x.shape[2]))
        if self.regularize:
            x = self.dropout(x)

        x = self.fc2(x)
        return x, state, attention_weights

    def reset_state(self, batch_size):
        return tf.zeros((batch_size, self.num_cells))