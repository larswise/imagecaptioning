import logging

import tensorflow as tf
import tensorflow.keras as keras

from config import Configuration


class Encoder(tf.keras.Model):
    def __init__(self, dimensions: int, config: Configuration):
        super(Encoder, self).__init__()
        self.use_regularization = config.use_regularization
        logging.info(f"Encoder -> regularization: {config.use_regularization}, dropout: {config.drop_amt}, regularization: {config.regularization_amt}, enc. activation: {config.encoder_activation}")
        if config.encoder_activation == 'relu':
            self.activation = tf.nn.relu
        elif config.encoder_activation == 'sigmoid':
            self.activation = tf.nn.sigmoid
        else:
            self.activation = None

        if config.use_regularization:
            self.dropout = keras.layers.Dropout(config.drop_amt)
            self.fcregularizer = tf.keras.regularizers.L1(l1=config.regularization_amt)
        else:
            self.fcregularizer = None
        self.fc = keras.layers.Dense(dimensions, activation=self.activation, activity_regularizer=self.fcregularizer)


    def call(self, x):
        x = self.fc(x)
        if self.use_regularization:
            x = self.dropout(x)
        return x


