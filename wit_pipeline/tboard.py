import tensorflow as tf

g = tf.Graph()

base_path = "E:\\WIT_10pct\\train"

train_name = "train_coco_efficientnet_-1_default_emb_dropout"

with g.as_default() as g:
    tf.train.import_meta_graph('./checkpoint/model.ckpt-240000.meta')

with tf.Session(graph=g) as sess:
    file_writer = tf.summary.FileWriter(logdir='checkpoint_log_dir/faceboxes', graph=g)