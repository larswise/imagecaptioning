import logging
import os
import time

import numpy as np
import tensorflow as tf
import nltk

from config import Configuration
from models.model import CaptionModel
from results import dump_predicted_caps, dump_scores, perform_scoring, dump_losses, load_scores, load_losses
from steps.step3_download_glove import download_glove_if_not_present
from steps.step4_text_processor import process_captions, sequence_generator, process_text, get_text_vectors
from steps.step5_vgg_model import load_func, process_img_npy_files

nltk.download('wordnet')

def start(cfg: Configuration, train_name):
    logging.info("Training is starting!")
    train_data_dir = os.path.join(cfg.root, 'train', f"{train_name}")
    if not os.path.exists(train_data_dir):
        if not os.path.exists(os.path.join(cfg.root, 'train')):
            os.mkdir(os.path.join(cfg.root, 'train'))
        os.mkdir(train_data_dir)

    # GLOVE
    download_glove_if_not_present(cfg)
    text_captions, filtered_df, images_folder, validation_folder = process_text(cfg)
    text_captions, caption_len = process_captions(text_captions)
    all_cap_len = [len(cap) for cap in text_captions]
    vocab, vectorizer = get_text_vectors(cfg, text_captions, caption_len)
    text_captions = sequence_generator(text_captions, vectorizer)
    images_npy, images, npy_dir = process_img_npy_files(cfg, filtered_df, images_folder, validation_folder)

    # først, flytt nødvendig stuff opp hit, så if coco... else...
    # filtered_df[filtered_df['type'] == 'val'].index
    BUFFER_SIZE = 1000
    BATCH_SIZE = cfg.batch_size
    logging.info(f"Batch size is {BATCH_SIZE}")
    if cfg.dataset == 'COCO':
        train_idxes = filtered_df[filtered_df['type'] == 'train'].index
        val_idxes = filtered_df[filtered_df['type'] == 'val'].index
        train_img, train_npy, train_cap, train_cap_len = [images[i] for i in train_idxes], [images_npy[i] for i in train_idxes], np.take(text_captions, train_idxes, axis=0), [all_cap_len[i] for i in train_idxes]
        val_img, val_npy, val_cap, val_cap_len = [images[i] for i in val_idxes], [images_npy[i] for i in val_idxes], np.take(text_captions, val_idxes, axis=0), [all_cap_len[i] for i in val_idxes]

        train_cap = train_cap.tolist()
        val_cap = val_cap.tolist()

        if cfg.num_samples > 0:
            train_img = train_img[:cfg.num_samples]
            train_npy = train_npy[:cfg.num_samples]
            train_cap = train_cap[:cfg.num_samples]
            train_cap_len = train_cap_len[:cfg.num_samples]

        train_cap_len, train_img, train_npy, train_cap = zip(*sorted(zip(train_cap_len, train_img, train_npy, train_cap)))

        if cfg.val_samples > 0:
            val_img = val_img[:cfg.val_samples]
            val_npy = val_npy[:cfg.val_samples]
            val_cap = val_cap[:cfg.val_samples]
            val_cap_len = val_cap_len[:cfg.val_samples]

        val_cap_len, val_img, val_npy, val_cap = zip(*sorted(zip(val_cap_len, val_img, val_npy, val_cap)))

        logging.info(f'Samples: {len(train_cap)}, val: {len(val_cap)}')

        dataset = tf.data.Dataset.from_tensor_slices((list(train_img), list(train_npy), list(train_cap))).map(lambda item0, item1, item2: tf.numpy_function(
            load_func, [item0, item1, item2], [tf.string, tf.float32, tf.int32]),
                                num_parallel_calls=tf.data.AUTOTUNE).shuffle(BUFFER_SIZE).batch(BATCH_SIZE).prefetch(
                                buffer_size=tf.data.experimental.AUTOTUNE)
        val_dataset = tf.data.Dataset.from_tensor_slices((list(val_img), list(val_npy), list(val_cap))).map(lambda item0, item1, item2: tf.numpy_function(
            load_func, [item0, item1, item2], [tf.string, tf.float32, tf.int32]),
                                num_parallel_calls=tf.data.AUTOTUNE).shuffle(BUFFER_SIZE).batch(BATCH_SIZE).prefetch(
                                buffer_size=tf.data.experimental.AUTOTUNE)
        num_batches = dataset.cardinality().numpy()
    else:
        if cfg.num_samples > 0:
            images = images[:cfg.num_samples]
            images_npy = images_npy[:cfg.num_samples]
            text_captions = text_captions[:cfg.num_samples]


        dataset = tf.data.Dataset.from_tensor_slices((images, images_npy, text_captions))
        dataset = dataset.map(lambda item0, item1, item2: tf.numpy_function(
            load_func, [item0, item1, item2], [tf.string, tf.float32, tf.int32]),
                              num_parallel_calls=tf.data.AUTOTUNE)

        num_batches = dataset.cardinality().numpy() // BATCH_SIZE
        val_size = int(BATCH_SIZE * (num_batches * cfg.train_val_split))
        val_dataset = dataset.take(val_size).shuffle(BUFFER_SIZE).batch(BATCH_SIZE).prefetch(
            buffer_size=tf.data.experimental.AUTOTUNE)
        dataset = dataset.skip(val_size).shuffle(BUFFER_SIZE).batch(BATCH_SIZE).prefetch(
            buffer_size=tf.data.experimental.AUTOTUNE)

    # method to process images from folder in batches, and save them to disk as .npy files
    # embeddings = get_embeddings(cfg, vocab, glove_vectors)
    # Get feature embeddings
    # embedding_model = define_model(cfg, embeddings, vocab)
    # feature_embeddings = get_features(embedding_model, vectorizer, list(filtered_df[cfg.text_col]))
    #del glove_vectors
    #gc.collect()
    embedding_dim = cfg.embedding_dim
    units = cfg.rnn_units
    if cfg.conv_model == "efficientnet":
        img_shape = 49 # efficientnet is 7x7x2048
    elif cfg.conv_model == "inception_v3":
        img_shape = 64 # inception is 8x8x2048

    logging.info(f"Done pre-processing, instantiate model. Vocabulary size is {vectorizer.vocabulary_size()}")
    model, ckpt_manager, start_epoch = CaptionModel.load_or_init(embedding_dim, img_shape, units, vectorizer, cfg, train_data_dir)
    EPOCHS = cfg.epochs
    loss_plot, loss_val_plot = load_losses(train_data_dir)
    scores = load_scores(train_data_dir)
    idx_to_word = {v: k for k, v in vocab.items()}
    logging.info(f"Start training {EPOCHS} epochs. Starting from from epoch #{start_epoch}")

    for epoch in range(start_epoch, EPOCHS):
        start = time.time()
        total_loss = 0
        total_val_loss = 0
        current_epoch = epoch + 1

        #################
        #     TRAIN     #
        #################
        logging.info(f"Epoch {current_epoch} train. Use regularization: {model.use_regularization}, Optimizer: {cfg.optimizer}, lr: {cfg.learning_rate}")
        for (batch, (image_path, img_tensor, target)) in enumerate(dataset):
            if cfg.optimized_mode:
                batch_loss, t_loss = model.tf_train_step(img_tensor, target)
            else:
                batch_loss, t_loss = model.train_step(img_tensor, target)
            total_loss += t_loss
            if batch % 500 == 0:
                average_batch_loss = batch_loss.numpy() / int(target.shape[1])
                logging.info(f'Train: Epoch {current_epoch} Batch {batch} Loss {average_batch_loss:.4f}')

            if current_epoch == 1 and batch == 0: # add the very first loss calc
                loss_plot.append(average_batch_loss)

        loss_plot.append(total_loss.numpy() / num_batches)
        if current_epoch % 5 == 0:
            logging.info(f"Saving checkpoint at epoch {current_epoch}")
            ckpt_manager.save(current_epoch)
        ####################
        #    VALIDATION    #
        ####################
        # since we have already batched val_dataset, its cardinality will be the number of batches
        logging.info(f"Starting validation step. Samples: {val_dataset.cardinality().numpy()*BATCH_SIZE}")

        for (batch, (image_path, img_tensor, target)) in enumerate(val_dataset):
            if cfg.optimized_mode:
                batch_val_loss, t_val_loss = model.tf_train_step(img_tensor, target, True)
            else:
                batch_val_loss, t_val_loss = model.train_step(img_tensor, target, True)
            total_val_loss += t_val_loss

            if batch % 250 == 0:
                average_batch_val_loss = batch_val_loss.numpy() / int(target.shape[1])
                logging.info(f'Epoch {current_epoch} Validation Batch {batch} Loss {average_batch_val_loss:.4f}')

            if current_epoch == 1 and batch == 0: # add the very first loss calc
                loss_val_plot.append(average_batch_val_loss)

        num_val_batches = val_dataset.cardinality().numpy()
        loss_val_plot.append(total_val_loss.numpy() / num_val_batches)

        if current_epoch % 5 == 0 or current_epoch == 1:
            logging.info("Dump losses to file...")
            dump_losses(loss_plot, loss_val_plot, train_data_dir)
            logging.info("Start scoring step for validation data...")
            bleus_meteor, predicted_caps = perform_scoring(val_dataset, model, caption_len, idx_to_word, BATCH_SIZE)
            logging.info("Start scoring step for training data...")
            bleus_meteor_train, predicted_caps_train = perform_scoring(dataset, model, caption_len, idx_to_word, BATCH_SIZE)
            logging.info("Scoring step completed.")
            logging.info(f"Bleu/Meteor val..: {bleus_meteor}")
            logging.info(f"Bleu/Meteor train..: {bleus_meteor_train}")
            scores.append((bleus_meteor_train, bleus_meteor))
            logging.info("Dump scores to file...")
            dump_scores(scores, train_data_dir)
            dump_predicted_caps((predicted_caps, predicted_caps_train), train_data_dir, current_epoch)

        logging.info(f'Epoch {current_epoch} Train loss {total_loss / num_batches:.6f} - Validation loss {total_val_loss / num_val_batches:.6f}')
        logging.info(f"Loss..: {loss_plot} - Val..: {loss_val_plot}")
        logging.info(f'Time taken for 1 epoch {time.time() - start:.2f} sec\n')
        logging.info(f'End of epoch {current_epoch} - shuffle train and validation dataset')
        val_dataset.shuffle(num_val_batches)
        dataset.shuffle(num_batches)
        logging.info("--------------------------------------------------------")
    logging.info(f"Training is complete, saving train and validation loss to pickle file")
    logging.info("Training is done!")
