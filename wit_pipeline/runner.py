import argparse
import datetime
import logging

import values
from config import Configuration
from main import start
from results import load_captions

if __name__ == '__main__':


    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    parser = argparse.ArgumentParser(description='Image captioning, encoder decoder with attention.')
    parser.add_argument('--dataset', type=str, help='Dataset-name, ex: WIT, COCO')
    parser.add_argument('--train_name', type=str, default=None, help='Name of train job, will be name of folder')
    parser.add_argument('--optimizer', type=str, help='Which optimizer to use: adam, sgd, rmsprop')
    parser.add_argument('--conv_model', type=str, help='Which CNN model to use, efficientnet or inception_v3')
    parser.add_argument('--decoder_fc_neurons', type=str, help='Number of neurons on last fc layer in the decoder')
    parser.add_argument('--encoder_activation', type=str, help='relu, sigmoid...')
    parser.add_argument('--use_glove', default=0, type=int, help='Use glove embeddings or default')
    parser.add_argument('--optimized_mode', default=0, type=int, help='Use @tf.function')
    parser.add_argument('--max_sentence_length', type=int, help='length of sequence')
    parser.add_argument('--num_samples', type=int, help='number of training samples to use')
    parser.add_argument('--val_samples', type=int, help='number of validation samples to use')
    parser.add_argument('--epochs', type=int, help='number of training epochs')
    parser.add_argument('--embedding_dim', type=int, help='number of dim when not using glove')
    parser.add_argument('--batch_size', type=int, help='size of the training batches')
    parser.add_argument('--use_regularization', type=int, help='Use regulariazation (1) or dont use it (0)')
    parser.add_argument('--rnn_units', type=int, help='Number of recurrent units in GRU')
    parser.add_argument('--drop_amt', type=float, help='amount of dropout, 0 - .5')
    parser.add_argument('--reularization_amt', type=float, help='regularization amount, 0 - .5')
    parser.add_argument('--train_val_split', type=float, help='validation split, default 0.2')
    parser.add_argument('--learning_rate', type=float, help='learning rate, optional, defaults to 0.01')

    args = parser.parse_args()
    config_vals = values.values
    if args.__dict__:
        parsed_args = {k: v for k, v in args.__dict__.items() if v is not None}
        if 'optimized_mode' in parsed_args:
            parsed_args['optimized_mode'] = bool(parsed_args['optimized_mode'])
        if 'use_glove' in parsed_args:
            parsed_args['use_glove'] = bool(parsed_args['use_glove'])
        if 'use_regularization' in parsed_args:
            parsed_args['use_regularization'] = bool(parsed_args['use_regularization'])

        config_vals.update(parsed_args)

    cfg = Configuration(**config_vals)
    use_rgl = "rglz" if cfg.use_regularization else "no_rglz"
    embedding = f'{("glove" if cfg.use_glove else "default")}_emb'
    now_str = datetime.datetime.now().strftime('%d%m-%H-%H-%S')
    if parsed_args.get("train_name", None) is not None:
        training_name = parsed_args["train_name"]
    else:
        training_name = f'{now_str}_{cfg.dataset}_{cfg.optimizer}_{use_rgl}_{cfg.conv_model}_{cfg.num_samples}_{embedding}'.lower()

    a = load_captions(cfg, "_result_coco_adam_regl_glovex256x128")
    start(cfg, training_name)
