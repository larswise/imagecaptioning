class Configuration():

    def __init__(self, **kwargs):
        for arg in kwargs:
            setattr(self, f"_{arg}", kwargs[arg])

    @property
    def root(self):
        return self._root

    @property
    def en_tsv(self):
        return self._en_tsv

    @property
    def num_samples(self):
        return self._num_samples

    @property
    def val_samples(self):
        return self._val_samples

    @property
    def base_tsv(self):
        return self._base_tsv

    @property
    def data_filter(self):
        return self._data_filter

    @property
    def npy_dir(self):
        return self._npy_dir

    @property
    def dataset(self):
        return self._dataset

    @property
    def text_col(self):
        return self._text_col

    @property
    def glove_dim(self):
        return self._glove_dim

    @property
    def embedding_dim(self):
        return self._embedding_dim

    @property
    def max_sentence_length(self):
        return self._max_sentence_length

    @property
    def max_vocab_size(self):
        return self._max_vocab_size

    @property
    def imgs(self):
        return self._imgs

    @property
    def img_dims(self):
        return self._img_dims

    @property
    def use_glove(self):
        return self._use_glove

    @property
    def train_val_split(self):
        return self._train_val_split

    @property
    def epochs(self):
        return self._epochs

    @property
    def optimized_mode(self):
        return self._optimized_mode

    @property
    def batch_size(self):
        return self._batch_size

    @property
    def use_regularization(self):
        return self._use_regularization

    @property
    def conv_model(self):
        return self._conv_model

    @property
    def optimizer(self):
        return self._optimizer

    @property
    def learning_rate(self):
        return self._learning_rate

    @property
    def rnn_units(self):
        return self._rnn_units

    @property
    def drop_amt(self):
        return self._drop_amt

    @property
    def regularization_amt(self):
        return self._reularization_amt

    @property
    def decoder_fc_neurons(self):
        return self._decoder_fc_neurons

    @property
    def encoder_activation(self):
        return self._encoder_activation



