import logging
import os
import pickle

import numpy as np
import tensorflow as tf

from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.meteor_score import single_meteor_score

from config import Configuration
from models.model import CaptionModel
from steps.step1b_mscoco import val_cocodataset_to_dataframe


def perform_scoring(val_dataset: tf.data.Dataset, model: CaptionModel, caption_len, idx_to_word, BATCH_SIZE: int):
    bleu1 = bleu2 = bleu3 = bleu4 = meteor = 0
    val_count = 0
    if os.sep == '\\':
        batches_to_score = 15
        batches_to_cap = 10
    else:
        batches_to_score = 75
        batches_to_cap = 20
    random_batch = np.random.randint(0, val_dataset.cardinality().numpy(), batches_to_cap).tolist()
    logging.info(f"Score {batches_to_score}, caption {batches_to_cap} samples. Batches: {random_batch}")
    predicted_caps = []
    for batch, (b_path, b_img, b_cap) in enumerate(val_dataset.take(batches_to_score)):
        random_out = np.random.randint(0, b_path.shape[0], 10).tolist()
        logging.info(f"Save predicted caps for these samples in the batch: {random_out}")
        references = []
        hypotheses = []
        if batch % 10 == 0:
            logging.info(f'Scored {batch} batches')
        for i in range(0, b_path.shape[0]):
            path, img, cap = b_path[i], b_img[i], b_cap[i]
            preds, att_plots = model.evaluate(img, caption_len)
            if preds[-1] == '<EOS>':
                preds = preds[:-1]  # omit eos
            if len(preds) > 1:
                if i in random_out and batch in random_batch:
                    predicted_caps.append((path, preds, att_plots))
                    print(f"Predicted for {path}: {preds}")
                actual = [idx_to_word[w] for w in cap.numpy() if w != 0]
                actual = actual[1:-1]
                hypotheses.append(preds)
                references.append(actual)
                val_count += 1

            if val_count > 1 and val_count % 1000 == 0:
                logging.info(f"Scoring processed {val_count} samples.")

        if len(hypotheses) > 0:
            bleu1 += sum([sentence_bleu([ref], hyp, (1.0, 0, 0, 0)) for ref, hyp in zip(references, hypotheses)])
            bleu2 += sum([sentence_bleu([ref], hyp, (0.5, 0.5, 0, 0)) for ref, hyp in zip(references, hypotheses)])
            bleu3 += sum([sentence_bleu([ref], hyp, (0.33, 0.33, 0.33, 0)) for ref, hyp in zip(references, hypotheses)])
            bleu4 += sum([sentence_bleu([ref], hyp, (0.25, 0.25, 0.25, 0.25)) for ref, hyp in zip(references, hypotheses)])
            meteor += sum([round(single_meteor_score(ref, hyp), 5) for ref, hyp in zip(references, hypotheses)])

    bleus_meteor = np.array([bleu1, bleu2, bleu3, bleu4, meteor]) / (val_count)
    print(bleus_meteor)
    return bleus_meteor, predicted_caps

def dump_losses(loss_plot, loss_val_plot, train_data_dir):
    with open(os.path.join(train_data_dir, f'loss.lst'), 'wb') as pickle_file:
        pickle.dump(loss_plot, pickle_file)

    with open(os.path.join(train_data_dir, f'loss_val.lst'), 'wb') as pickle_val_file:
        pickle.dump(loss_val_plot, pickle_val_file)

def dump_predicted_caps(predicted_caps, train_data_dir, current_epoch):
    with open(os.path.join(train_data_dir, f'preds_epoch_{current_epoch}.lst'), 'wb') as pickle_file:
        pickle.dump(predicted_caps, pickle_file)

def load_losses(train_data_dir):
    try:
        with open(os.path.join(train_data_dir, f'loss_val.lst'), 'rb') as pickle_val_file:
            val_plot = pickle.load(pickle_val_file)

        with open(os.path.join(train_data_dir, f'loss.lst'), 'rb') as pickle_file:
            loss_plot = pickle.load(pickle_file)
        return loss_plot, val_plot
    except Exception as ex:
        return [], []

def dump_scores(scores, train_data_dir):
    with open(os.path.join(train_data_dir, f'scores.lst'), 'wb') as pickle_file:
        pickle.dump(scores, pickle_file)


def load_scores(train_data_dir):
    try:
        with open(os.path.join(train_data_dir, f'scores.lst'), 'rb') as pickle_file:
            scores = pickle.load(pickle_file)
        return scores
    except Exception as ex:
        return []

def load_captions(cfg: Configuration, train_data_dir: str, epoch: int = None):
    try:
        # missing:
        val_imgs = os.path.join(cfg.root, 'val2014')

        path: str
        if epoch is not None:
            path = os.path.join(cfg.root, "train", train_data_dir, f'preds_epoch_{epoch}.lst')
        else:
            for i in range(0, 100, 5):
                epoch = i if i > 0 else 1
                path = os.path.join(cfg.root, "train", train_data_dir, f'preds_epoch_{i+5}.lst')
                if not os.path.isfile(path):
                    path = os.path.join(cfg.root, "train", train_data_dir, f'preds_epoch_{epoch}.lst')
                    break

        print(f"loading {path}")
        with open(path, 'rb') as pickle_file:
            predictions = pickle.load(pickle_file)
            # predictions are (path, preds, att_plots)
            # but some of the old are without att_plots...
            # so
            shape_check = np.array(predictions)
            print(shape_check.shape)
            if len(predictions[0]) == 2:
                predictions_val = [[pred[0].numpy().decode().replace('/cluster/work/larshanu/ic/val2014', val_imgs), pred[1]] for pred in predictions]
                predictions_train = None
            elif type(predictions) is tuple: # new format with both train and val caps in a tuple
                pred_val = predictions[0]
                pred_train = predictions[1]
                predictions_val = [[pred[0].numpy().decode().replace('/cluster/work/larshanu/ic/val2014', val_imgs), pred[1], pred[2]] for pred in pred_val]
                predictions_train = [[pred[0].numpy().decode().replace('/cluster/work/larshanu/ic/val2014', val_imgs), pred[1], pred[2]] for pred in pred_train]
            else:
                predictions_val = [[pred[0].numpy().decode().replace('/cluster/work/larshanu/ic/val2014', val_imgs), pred[1], pred[2]] for pred in predictions]
                predictions_train = None

            annotation_folder = "annotations"
            annotation_val_file = os.path.join(cfg.root, annotation_folder, 'captions_val2014.json')
            val_df = val_cocodataset_to_dataframe(cfg, annotation_val_file)
            return predictions_val, predictions_train, val_df

    except Exception as ex:
        print(ex)
        return None

