
values = {
    'root': 'E:\WIT_10pct',
    'en_tsv': 'en_df.tsv',
    'base_tsv': 'wit_v1.train.all-00000-of-00010.tsv',
    'data_filter': 'en',
    'npy_dir': 'npy_files',
    'text_col': 'page_title',
    'max_vocab_size': 40000,
    'max_sentence_length': 50,
    'img_dims': (224, 224),
    'num_samples': -1, # -1 means ALL
    'val_samples': -1,
    'glove_dim': 300,
    'embedding_dim': 256,
    'drop_amt': .4, # amount of dropout, global
    'reularization_amt': .05,
    'decoder_fc_neurons': 1024,
    'use_glove': False,
    'imgs': 'images',#'train2014',#or images
    'dataset': 'COCO',
    'train_val_split': 0.2, # 80% train, 20% val,
    'epochs': 3,
    'optimized_mode': True,
    'batch_size': 64,
    'use_regularization': True,
    'optimizer': 'adam', # or sgd, or rmsprop
    'learning_rate': 0.01,
    'rnn_units': 512,
    'conv_model': 'efficientnet',
    'encoder_activation': 'sigmoid'
}