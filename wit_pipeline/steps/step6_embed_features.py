from typing import Dict

import numpy as np

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import Input
from tensorflow.python.keras.layers import Embedding, TextVectorization

from config import Configuration


def define_model(config: Configuration, embeddings, vocab: Dict[str, int]):
    int_sequences_input = Input(shape=(None,), dtype="int64")
    embedding_layer = Embedding(
        len(vocab)+2,
        config.glove_dim,
        embeddings_initializer=keras.initializers.Constant(embeddings),
        trainable=False,
    )
    embedded_sequences = embedding_layer(int_sequences_input)
    model = keras.Model(int_sequences_input, embedded_sequences)
    model.compile()
    return model

def get_features(embedding_model: keras.Model, vectorizer: TextVectorization, raw_input_features):
    processed_features = vectorizer(np.array([[s] for s in raw_input_features])).numpy()
    return embedding_model.predict(processed_features)