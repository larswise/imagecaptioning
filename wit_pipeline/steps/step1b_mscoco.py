import json
import logging
import os
import random

import tensorflow as tf
import pandas as pd
from config import Configuration


def download_mscoco(config: Configuration):
    '''
    download if not present
    :return: data frame
    '''
    annotation_folder = 'annotations'
    annotation_file = os.path.join(config.root, annotation_folder, 'captions_train2014.json')
    annotation_val_file = os.path.join(config.root, annotation_folder, 'captions_val2014.json')
    annotations_test_file = os.path.join(config.root, annotation_folder, 'image_info_test2014.json')
    if not os.path.exists(os.path.join(config.root, annotation_folder)):
        annotation_zip = tf.keras.utils.get_file('captions.zip',
                                                 cache_subdir=config.root,
                                                 origin='http://images.cocodataset.org/annotations/annotations_trainval2014.zip',
                                                 extract=True)
        os.remove(annotation_zip)

    # Download image files
    image_folder = 'train2014'
    IMAGES_PATH = os.path.join(config.root, image_folder)
    if not os.path.exists(IMAGES_PATH):
        image_zip = tf.keras.utils.get_file('train2014.zip',
                                            cache_subdir=config.root,
                                            origin='http://images.cocodataset.org/zips/train2014.zip',
                                            extract=True)
        os.remove(image_zip)

    image_val_folder = 'val2014'
    VAL_IMAGES_PATH = os.path.join(config.root, image_val_folder)
    if not os.path.exists(VAL_IMAGES_PATH):
        image_val_zip = tf.keras.utils.get_file('val2014.zip',
                                                cache_subdir=config.root,
                                                origin='http://images.cocodataset.org/zips/val2014.zip',
                                                extract=True)
        os.remove(image_val_zip)

    return IMAGES_PATH, VAL_IMAGES_PATH, annotation_file, annotation_val_file, annotations_test_file


def cocodataset_to_dataframe(config: Configuration, captions_file, captions_val_file, captions_test_file = None):
    existing_file_name = f"{config.dataset.lower()}_{config.num_samples}.tsv"

    #if os.path.exists(os.path.join(config.root, existing_file_name)):
    #    return pd.read_csv(os.path.join(config.root, existing_file_name), sep='\t', encoding='utf8')
    with open(captions_file, 'r') as f, open(captions_val_file, 'r') as v:
        annotations = json.load(f)
        annotations_val = json.load(v)

        if config.num_samples > 0:
            annots = list(annotations['annotations'])
            random.shuffle(annots)
            captions = annots[:config.num_samples]
            vals = list(annotations_val['annotations'])
            random.shuffle(vals) # scary shit, inference wont work because vocabulary will be different every time...
            captions_val = list(vals[:int(config.num_samples*config.train_val_split)])
        else:
            captions = list(annotations['annotations'])
            captions_val = list(annotations_val['annotations'])
            random.shuffle(captions_val)
            random.shuffle(captions)

        i = 0
        dict_list = []
        for cap in captions:
            if i % 10000 == 0:
                logging.info(f"Loaded {i} rows...")
            dict_list.append({ config.text_col: cap['caption'], 'type': 'train', 'image': 'COCO_train2014_' + '%012d.jpg' % (cap['image_id'])})
            i += 1
        i = 0
        for cap in captions_val:
            if i % 10000 == 0:
                logging.info(f"Loaded {i} rows...")
            dict_list.append({ config.text_col: cap['caption'], 'type': 'val', 'image': 'COCO_val2014_' + '%012d.jpg' % (cap['image_id'])})
            i += 1

        df = pd.DataFrame(dict_list, columns=[config.text_col, 'type', 'image'])
        df.reset_index(drop=True, inplace=True)
        df.to_csv(os.path.join(config.root, existing_file_name), sep='\t')
        return df

def val_cocodataset_to_dataframe(config: Configuration, captions_val_file):
    existing_file_name = f"{config.dataset.lower()}_{config.num_samples}.tsv"
    #if os.path.exists(os.path.join(config.root, existing_file_name)):
    #    return pd.read_csv(os.path.join(config.root, existing_file_name), sep='\t', encoding='utf8')
    with open(captions_val_file, 'r') as v:
        annotations_val = json.load(v)
        captions_val = list(annotations_val['annotations'])
        random.shuffle(captions_val)
        i = 0
        dict_list = []
        for cap in captions_val:
            if i % 10000 == 0:
                logging.info(f"Loaded {i} rows...")
            dict_list.append({ config.text_col: cap['caption'], 'type': 'val', 'image': 'COCO_val2014_' + '%012d.jpg' % (cap['image_id'])})
            i += 1

        df = pd.DataFrame(dict_list, columns=[config.text_col, 'type', 'image'])
        df.reset_index(drop=True, inplace=True)
        df.to_csv(os.path.join(config.root, existing_file_name), sep='\t')
        return df



def load_test_images(cfg: Configuration, test_folder, amt: int):
    test_images = os.listdir(os.path.join(cfg.root, test_folder))
    random.shuffle(test_images)
    return test_images[:amt]

