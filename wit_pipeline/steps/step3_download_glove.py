import asyncio
import logging
import os
import numpy as np

from contextlib import closing
from zipfile import ZipFile

import aiohttp


from config import Configuration

GLOVE_URL = 'http://nlp.stanford.edu/data/glove.6B.zip'
filename = GLOVE_URL.split('/')[-1]

def download_glove_if_not_present(config: Configuration):
    if os.path.isfile(os.path.join(config.root, filename)):
        logging.info(f"Glove Embeddings: {filename} already exists, no need to download")
        return # we already did this stuff...

    with closing(asyncio.get_event_loop()) as loop, \
            closing(aiohttp.ClientSession()) as session:
        download_task = get_file(session, config)
        loop.run_until_complete(download_task)
        loop.close()

    return


async def get_file(session, config: Configuration):
    file_path = os.path.join(config.root, filename)
    async with session.get(GLOVE_URL) as g_response:
        glove_zip = await g_response.read()

        try:
            with open(file_path, 'wb') as f:
                f.write(glove_zip)
        except Exception as ex:
            logging.info(f"Failed to save: {ex}")
        finally:
            await session.close()

    if os.path.isfile(file_path):
        ZipFile(file_path).extractall(config.root)


def load_glove_vectors(config: Configuration):
    glove = {}
    glove_file = f"{filename.rsplit('.', 1)[0]}.{config.glove_dim}d.txt"
    try:
        if os.path.isfile(os.path.join(config.root, glove_file)):
            with open(os.path.join(config.root, glove_file), encoding='utf8') as glove_embeddings:
                for line in glove_embeddings:
                    word, vec = line.split(maxsplit=1)
                    vec = np.fromstring(vec, "f", sep=" ")
                    glove[word] = vec
    except Exception as ex:
        logging.error(f"Error parsing line: {ex} - Line: {line}")
    return glove
