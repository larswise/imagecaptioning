import io
import os
import posixpath
from urllib.parse import urlsplit, unquote
from PIL import Image

import pandas as pd
import asyncio
import logging
from contextlib import closing
import aiohttp

from config import Configuration


def download_images(df: pd.DataFrame, cfg: Configuration, start: int = 0):
    contents = os.listdir(os.path.join(cfg.root, cfg.imgs))
    if len(contents) != 0:
        return # we already have the images...

    with closing(asyncio.get_event_loop()) as loop,  \
        closing(aiohttp.ClientSession()) as session:
        semaphore = asyncio.Semaphore(3)
        download_tasks = (download(i, df.shape[0], df, session, semaphore, cfg) for i in range(start, df.shape[0]))
        logging.info("Start photo download")

        response = loop.run_until_complete(asyncio.gather(*download_tasks))
        loop.close()

    return response


async def download(index: int, max: int, df: pd.DataFrame, session, semaphore, cfg: Configuration):
    async with semaphore:
        try:
            img_url = df.iloc[index]['image_url']
            uuid = df.iloc[index]['image']
            filename = url2filename(img_url)
            logging.info(f"download {filename}")
            save_filename = f"{uuid}.{filename.rsplit('.', maxsplit=1)[1]}"
            success = True
            async with session.get(img_url) as im_response:
                img = await im_response.read()
                try:
                    img = Image.open(io.BytesIO(img))
                    img = img.resize(cfg.img_dims)
                    img.save(os.path.join(cfg.root, cfg.imgs, save_filename))
                except Exception as ex:
                    logging.info(f"Failed to save {save_filename}: {ex}")
                    #img = Image.open(os.path.join(cfg.root, 'empty_image.jpg'))
                    #img.save(os.path.join(cfg.root, cfg.imgs, save_filename))
                    success = False
                finally:
                    await asyncio.sleep(.1)
                    if max - index == 1:
                        await session.close()

        except Exception as ex:
            logging.info(f"error downloading image: {img_url}, retry from this index after a short break: {ex}")
            success = False
    return save_filename, index, success


def url2filename(url):
    """Return basename corresponding to url.
    Traceback (most recent call last):
    ...
    ValueError
    """
    urlpath = urlsplit(url).path
    basename = posixpath.basename(unquote(urlpath))
    if (os.path.basename(basename) != basename or
        unquote(posixpath.basename(urlpath)) != basename):
        raise ValueError  # reject '%2f' or 'dir%5Cbasename.ext' on Windows
    return basename