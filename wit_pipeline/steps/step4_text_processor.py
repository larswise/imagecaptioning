import os
import pickle
import re
import string
from typing import Dict, Tuple

import logging
from uuid import uuid4

import regex
import tensorflow as tf
import numpy as np
from numpy import savetxt, loadtxt
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from steps.step1_filterdata import save_filtered, filter_data
from steps.step1b_mscoco import cocodataset_to_dataframe, download_mscoco
from steps.step2_imgdownloader import download_images

START_CHAR = "<START>"
END_CHAR = "<EOS>"

def get_text_vectors(config, text_captions, captions_len) -> (dict, TextVectorization):
    vectorizer = TextVectorization(max_tokens=config.max_vocab_size,
                                   standardize=None,
                                   output_sequence_length=captions_len)
    text_ds = tf.data.Dataset.from_tensor_slices(text_captions).batch(128)
    vectorizer.adapt(text_ds)
    vocabulary = vectorizer.get_vocabulary()

    return dict(zip(vocabulary, range(len(vocabulary)))), vectorizer

def process_captions(text_captions, add_start_end: bool = True):
    for i, caption in enumerate(text_captions):
        caption = regex.sub(r'[[:punct:]]+', '', caption)
        caption = caption.lower().strip(string.punctuation)
        caption = [w for w in caption.split() if len(w) > 1 and w.isalpha()]
        caption = ' '.join(caption)
        if add_start_end:
            text_captions[i] = "{0} {1} {2}".format(START_CHAR, caption, END_CHAR)
        else:
            text_captions[i] = caption
    tokenized_caps = [sent.split() for sent in text_captions]
    longest = max(tokenized_caps, key=lambda coll: len(coll))
    return text_captions, len(longest)



def get_embeddings(config,
                   vocab: Dict[str, int],
                   glove_dict: Dict[str, any]):
    features_file = f'embeddings_{config.num_samples}_{config.dataset}.csv'
    features_full_path = os.path.join(config.root, features_file)
    if os.path.isfile(features_full_path):
        return loadtxt(features_full_path, delimiter=',')

    hits = 0
    misses = 0

    matrix = np.zeros((len(vocab), config.glove_dim))
    vocab = { k: i for i, k in enumerate(vocab) }
    for w, i in vocab.items():
        glove_vector = glove_dict.get(w)
        if glove_vector is not None:
            matrix[i] = glove_vector
            hits += 1
        elif w == '':
            matrix[i] = np.zeros(shape=(300,))
            misses += 1
        elif w == '<START>':
            matrix[i] = np.ones(shape=(300,))
            misses += 1
        elif w == '<EOS>':
            matrix[i] = np.negative(np.ones(shape=(300,)))
            misses += 1
        else:
            matrix[i] = np.random.uniform(-1, 1, config.glove_dim)
            misses += 1

    logging.info(f"Hits: {hits}, Misses: {misses}")
    savetxt(features_full_path, matrix, delimiter=',')
    return matrix


def sequence_generator(text_captions: np.ndarray, vectorizer: TextVectorization):
    captions_as_seq = vectorizer(text_captions)
    return captions_as_seq.numpy().tolist()

def process_text(cfg) -> Tuple:
    validation_folder = None #not used for WIT
    images_folder = None
    if cfg.dataset == 'WIT':
        logging.info("filter data")
        filtered_df, save = filter_data(cfg)
        filtered_df['type'] = 'train'
        img_response = download_images(filtered_df, cfg)
        if img_response is not None:
            rows = filtered_df.index[[t[1] for t in img_response if not t[2]]]
            filtered_df = filtered_df.drop(rows)
            logging.info("Successfully downloaded images")
        if save:
            logging.info("saving non existent filtered file")
            save_filtered(cfg, filtered_df)
    else:
        images_folder, validation_folder, captions_file, captions_val_file, captions_test_file = download_mscoco(cfg)
        filtered_df = cocodataset_to_dataframe(cfg, captions_file, captions_val_file, captions_test_file)

    text_captions = filtered_df[cfg.text_col].values

    return text_captions, filtered_df, images_folder, validation_folder













