import logging
import os
from uuid import uuid4

import pandas as pd

from config import Configuration


def filter_data(config: Configuration) -> (pd.DataFrame, bool):
    try:
        if os.path.isfile(os.path.join(config.root, config.en_tsv)):
            df = pd.read_csv(os.path.join(config.root, config.en_tsv), sep='\t', encoding='utf8')
            logging.info(f'Loaded existing file of shape {df.shape}')
            return df, False

        df = pd.read_csv(os.path.join(config.root, config.base_tsv), sep='\t', encoding='utf8')
        df = df[(df['language'] == config.data_filter) & (df['image_url'].str.endswith('.jpg')) & (df[config.text_col].str.len() > 18)]
        if config.num_samples == -1:
            df = df.sample(frac=abs(config.num_samples))
        else:
            df = df.sample(config.num_samples)
        df.reset_index(inplace=True)
        df['image'] = df.apply(lambda _: str(uuid4()), axis=1)
        return df, True
    except Exception as ex:
        raise ex


def save_filtered(config: Configuration, df: pd.DataFrame):
    df.to_csv(os.path.join(config.root, config.en_tsv), sep='\t')