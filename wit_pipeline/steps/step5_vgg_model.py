import logging
import os
import re

import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow.keras.models import Model

from config import Configuration


def load_cnn_model(config: Configuration):
    if config.conv_model == 'vgg19':
        base_model = tf.keras.applications.vgg19.VGG19(
            include_top=False, weights='imagenet', input_tensor=None,
            input_shape=None, pooling=None,
            classifier_activation='softmax')
        max_pooling = base_model.layers[-1]
        return Model(base_model.layers[0].input, max_pooling.output)
    elif config.conv_model == 'efficientnet':
        base_model = tf.keras.applications.efficientnet.EfficientNetB0(
            include_top=False, weights='imagenet'
        )
        last_layer = base_model.layers[-1]
        return Model(base_model.layers[0].input, last_layer.output)
    elif config.conv_model == 'inception_v3':
        base_model = tf.keras.applications.inception_v3.InceptionV3(
            include_top=False, weights='imagenet')
        last_layer = base_model.layers[-1]
        return Model(base_model.layers[0].input, last_layer.output)
    else:
        return None

def load_func(image_path, image, caption):
    img_tensor = np.load(image.decode('utf-8')).astype(np.float32)
    return image_path, img_tensor, caption.astype(np.int32)

def extract_image_features(config: Configuration, model: Model, images, npy_dir: str):
    preprocess_step = None
    if config.conv_model == 'vgg19':
        preprocess_step = load_image_vgg
    elif config.conv_model == 'efficientnet':
        preprocess_step = load_image_effnet
    elif config.conv_model == 'inception_v3':
        preprocess_step = load_image_resnet50

    dataset = tf.data.Dataset.from_tensor_slices(images)
    dataset = dataset.map(
        preprocess_step, num_parallel_calls=tf.data.AUTOTUNE).batch(128)

    for image, path in dataset:
        batched_img_features = model(image)
        batched_img_features = tf.reshape(batched_img_features, (batched_img_features.shape[0], -1, batched_img_features.shape[3]))

        for features, path in zip(batched_img_features, path):
            path_of_feature = path.numpy().decode("utf-8")
            directory, filename = path_of_feature.rsplit(os.sep, 1)

            filename_wo_ext, _ = filename.rsplit('.', 1)
            np.save(os.path.join(config.root, npy_dir, f"{filename_wo_ext}.npy"), features.numpy())


def load_image_vgg(image_path: str):
    img = tf.io.read_file(image_path)
    img = tf.io.decode_jpeg(img, channels=3)
    w, d, c = img.shape
    if not w == d == 224:
        img = tf.image.resize(img, (224, 224))
    img = tf.keras.applications.vgg19.preprocess_input(img)
    return img, image_path

def load_image_effnet(image_path: str):
    img = tf.io.read_file(image_path)
    img = tf.io.decode_jpeg(img, channels=3)
    w, d, c = img.shape
    if not w == d == 224:
        img = tf.image.resize(img, (224, 224))
    img = tf.keras.applications.efficientnet.preprocess_input(img)
    return img, image_path

def load_image_resnet50(image_path: str):
    img = tf.io.read_file(image_path)
    img = tf.io.decode_jpeg(img, channels=3)
    w, d, c = img.shape
    if not w == d == 299:
        img = tf.image.resize(img, (299, 299))
    img = tf.keras.applications.inception_v3.preprocess_input(img)
    return img, image_path

def process_img_npy_files(cfg: Configuration, filtered_df: pd.DataFrame, images_folder: str, validation_folder: str):
    npy_dir = f"npy_files_{cfg.conv_model}_{cfg.dataset.lower()}_-1"
    if cfg.dataset == 'WIT':
        # images = [os.path.join(cfg.root, npy_dir, f"{name.split('.', 1)[0]}.jpg") for name in os.listdir(os.path.join(cfg.root, cfg.imgs))]
        images = [os.path.join(cfg.root, npy_dir, f"{name.split('.', 1)[0]}.jpg") for name in
                  os.listdir(os.path.join(cfg.root, npy_dir))]
    else:  # coco-dataset
        images = [os.path.join((images_folder if type == 'train' else validation_folder), val) for val, type in
                  zip(filtered_df['image'].values, filtered_df['type'].values)]

    conv_model = load_cnn_model(cfg)

    if not os.path.exists(os.path.join(cfg.root, npy_dir)):
        logging.info(f"Directory does NOT exists: {npy_dir}")
        os.mkdir(os.path.join(cfg.root, npy_dir))
        extract_image_features(cfg, conv_model, images, npy_dir)
    elif cfg.dataset == 'COCO':
        existing_npy_files = [os.path.splitext(os.path.basename(np_im))[0] for np_im in list(os.listdir(os.path.join(cfg.root, npy_dir)))]
        existing_images = [os.path.splitext(os.path.basename(im))[0] for im in list(os.listdir(os.path.join(cfg.root, images_folder)))]
        missing_npy_files = list(set(existing_images)-set(existing_npy_files))
        missing = [os.path.join(cfg.root, images_folder, f"{im}.jpg") for im in existing_images if im in missing_npy_files]
        if len(missing) > 0:
            extract_image_features(cfg, conv_model, missing, npy_dir)
        logging.info(f"Directory exists: {npy_dir}")

    if cfg.dataset == 'COCO':
        only_val = filtered_df[filtered_df['type'] == 'val']
        images_val = [os.path.join(validation_folder, val) for val in only_val['image'].values]
        images_val = list(set(images_val))
        check_first_exists = f"{images_val[0].rsplit(os.sep, 1)[1].rsplit('.', 1)[0]}.npy"
        if not os.path.exists(os.path.join(cfg.root, npy_dir, check_first_exists)):
            extract_image_features(cfg, conv_model, images_val, npy_dir)

    if cfg.dataset == 'WIT':
        images_npy = [os.path.join(cfg.root, npy_dir, name) for name in os.listdir(os.path.join(cfg.root, npy_dir))]
    else:
        images_npy = [os.path.join(cfg.root, npy_dir, f"{val.rsplit('.', 1)[0]}.npy") for val in
                      filtered_df['image'].values]

    return images_npy, images, npy_dir


