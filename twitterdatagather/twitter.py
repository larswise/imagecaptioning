import tweepy
import json

from config import Configuration
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

class TwitterStreamer(tweepy.Stream):

    analyser = SentimentIntensityAnalyzer()
    auth: tweepy.OAuthHandler
    stream: tweepy.Stream


    def on_status(self, status):
        print("got status %s" % status.text)

    def on_error(self, status_code):
        print('Error: ' + repr(status_code))
        return False

    def on_data(self, data):
        tweet = json.loads(data)
        if tweet is not None and 'media' in tweet["entities"]:
            polarity = self.analyser.polarity_scores(tweet["text"])
            media = [m for m in tweet["entities"]["media"] if m["type"] == 'photo']
            if .5 < polarity['compound'] > -.5 and media:
                print(polarity['compound'], "-", tweet["text"])
                print(media[0]["media_url_https"])
