
class Configuration():

    def __init__(self, **kwargs):
        for arg in kwargs:
            setattr(self, f"_{arg}", kwargs[arg])

    @property
    def api_key(self):
        return self._api_key

    @property
    def api_secret(self):
        return self._api_secret

    @property
    def api_access_token(self):
        return self._api_access_token

    @property
    def api_access_token_secret(self):
        return self._api_access_token_secret

    @property
    def tracks(self):
        return self._tracks

