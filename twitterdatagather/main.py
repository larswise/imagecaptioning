import argparse
import os
from enum import Enum


from config import Configuration
from twitter import TwitterStreamer
from values import config_values


def start():
    conf = Configuration(**config_values)
    streamer = TwitterStreamer(
        conf.api_key, conf.api_secret,
        conf.api_access_token, conf.api_access_token_secret
    )
    streamer.filter(track=conf.tracks, languages=['en'])


if __name__ == "__main__":
    start()